"use strict"


const express        = require("express"),
      mongoose       = require("mongoose"),
      passport       = require("passport"),
      bodyParser     = require("body-parser"),
      LocalStrategy  = require("passport-local"),
      expressSession = require("express-session"),
      methodOverride = require("method-override"),
      flash          = require("connect-flash");
      

const User        = require("./models/user"),
      Comment     = require("./models/comment"),
      Campground  = require("./models/campground");


const middleware = require("./middleware");


const campgroundsRoutes = require("./routes/campgrounds"),
      commentsRoutes    = require("./routes/comments"),
      indexRoutes       = require("./routes/index");


const seedDB = require("./seeds");

mongoose.connect("mongodb://localhost/yelp_camp");

let app = express();

module.exports = app;

// passport configuration
app.use(expressSession({
  secret: "some secret key",
  resave: false,
  saveUninitialized: false
}));

app.use(flash());

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use('/static/', express.static(__dirname + "/public"));

app.use(methodOverride("_method"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.set("view engine", "ejs");


// seedDB();

app.use((req, res, next) => {

  res.locals.currentUser = req.user;

  res.locals.errors = req.flash("error");
  res.locals.successes = req.flash("success");

  next();
});


app.use("/", indexRoutes);
app.use("/campgrounds/", campgroundsRoutes);
app.use("/campgrounds/:id/comments", commentsRoutes);


app.listen(3000, () => console.log("Server has started!"));