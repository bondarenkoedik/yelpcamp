"use strict"

const Campground = require("./models/campground"),
      Comment    = require("./models/comment");

function isLoggedIn (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }

  // req.session.next = req.originalUrl;
  req.flash("error", "You should be signed in to do that!");

  res.redirect('/signin');
};

function checkCampgroundOwnership(req, res, next) {
  if (req.isAuthenticated()) {
    
    Campground.findById(req.params.id, (err, campground) => {
      if (err) {
        req.flash("error", err);
        res.redirect("back");
      } else {
        
        if (campground.author._id.equals(req.user._id)) {
          req.campground = campground;
          next();
        } else {
          req.flash("error", "You haven't rights to do that!")
          res.redirect("back");
        }
      
      }
    });
  } else {
    req.flash("error", "You are not authorized to do that!");
    res.redirect("back");
  }
}

function checkCommentOwnership(req, res, next) {
  if (req.isAuthenticated()) {

    const campgroundId = req.params.id;
    const commentId = req.params.comment_id;

    Campground.findById(campgroundId, (err, campground) => {

      if (err) {
        req.flash("error", err);
        res.redirect("back");
      } else {

        Comment.findById(commentId, (err, comment) => {

          if (err) {
            req.flash("error", err);
            res.redirect("back");
          } else {
            
            if (comment.author._id.equals(req.user._id)) {
              req.campground = campground;
              req.comment = comment;
              next();
            } else {
              req.flash("error", err);
              res.redirect("back");
            }
          }

        });

      }

    });
  } else {
    req.flash("error", err);
    res.redirect("back");
  }
}

module.exports = {
  isLoggedIn,
  checkCampgroundOwnership,
  checkCommentOwnership
}