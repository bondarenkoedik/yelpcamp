"use strict"


const express = require("express");
const router  = express.Router({mergeParams: true});

const Campground = require("../models/campground"),
      Comment    = require("../models/comment");

const middleware = require("../middleware");


module.exports = router;


router.get("/", (req, res) => {

  Campground.find({}, (err, campgrounds) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      return res.render("campgrounds/index", { campgrounds })
    }
  });

});

router.post("/", middleware.isLoggedIn, (req, res) => {

  const name = req.body.name;
  const img = req.body.image;
  const description = req.body.description;

  Campground.create({
    name,
    img,
    description,
    author: req.user
  }, (err, campground) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      return res.redirect("/campgrounds");      
    }
  })
  
});

router.get("/new", middleware.isLoggedIn, (req, res) => {
  return res.render("campgrounds/new");
});

router.get("/:id", (req, res) => {
  
  const id = req.params.id;

  Campground.findById(id).populate("comments").exec((err, campground) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      return res.render("campgrounds/show", { campground });
    }
  });

});

// EDIT
router.get("/:id/edit", middleware.checkCampgroundOwnership, (req, res) => {

  const id = req.params.id;

  return res.render("campgrounds/edit", { campground: req.campground });

});

// UPDATE
router.put("/:id", middleware.checkCampgroundOwnership, (req, res) => {

  req.campground.name = req.body.campground.name;
  req.campground.img = req.body.campground.img;
  req.campground.description = req.body.campground.description;

  req.campground.save((err, campground) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      return res.redirect(`/campgrounds/${req.params.id}`);    
    }
  });

});

// DELETE
router.delete("/:id", middleware.checkCampgroundOwnership, (req, res) => {

  // delete all comments before

  req.campground.comments.forEach(comment => {
    
    Comment.findByIdAndRemove(comment, (err) => {
      if (err) {
        req.flash("error", err.message);
        return res.redirect("back");
      }
    });

  });

  req.campground.remove(err => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      req.flash("success", "Campground has been removed!");
      return res.redirect("/campgrounds")
    }
  });

});