"use strict"

const express = require("express");
const router  = express.Router({mergeParams: true});

const Campground = require("../models/campground"),
      Comment    = require("../models/comment");

const middleware = require("../middleware");


module.exports = router;


router.get("/new", middleware.isLoggedIn, (req, res) => {

  Campground.findById(req.params.id, (err, campground) => {

    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      return res.render("comments/new", { campground })
    }

  });

});

router.post("/", middleware.isLoggedIn, (req, res) => {

  Campground.findById(req.params.id, (err, campground) => {

    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      
      const comment = {
        text: req.body.comment.text,
        author: req.user
      };

      Comment.create(comment, (err, comment) => {
        
        campground.comments.push(comment);
        
        campground.save((err, campground) => {
          if (err) {
            req.flash("error", err.message);
            return res.redirect("back");
          } else {
            return res.redirect(`/campgrounds/${req.params.id}`);
          }
        });
      
      });
    
    }
  
  });

});

router.get("/:comment_id/edit", middleware.checkCommentOwnership, (req, res) => {

  const campgroundId = req.params.id;
  const commentId = req.params.comment_id;

  return res.render("comments/edit", { campground: req.campground, comment: req.comment });

});

router.put("/:comment_id", middleware.checkCommentOwnership, (req, res) => {

    Comment.findByIdAndUpdate(req.params.comment_id, req.body.comment, (err, comment) => {
      if (err) {
        req.flash("error", err.message);
        return res.redirect("back");
      } else {
        return res.redirect(`/campgrounds/${req.campground._id}`);
      }
    });

});

router.delete("/:comment_id", middleware.checkCommentOwnership, (req, res) => {

  req.comment.remove((err) => {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    } else {
      req.flash("success", "Comment deleted!");
      return res.redirect(`/campgrounds/${req.campground._id}`);
    }
  });

});