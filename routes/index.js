"use strict"

const express  = require("express"),
      passport = require("passport");

const router  = express.Router();

const User = require("../models/user");


module.exports = router;


router.get("/", (req, res) => {
  return res.render("landing");
});

router.get("/signup", (req, res) => {
  return res.render("signup");
});

router.post("/signup", (req, res) => {

  const newUser = new User({username: req.body.username});

  User.register(newUser, req.body.password, (err, user) => {

    if (err) {
      req.flash("error", err.message);
      // return res.redirect("back"); 
      return res.render("signup"); // only second time
    }

    passport.authenticate("local")(req, res, () => {
      req.flash("success", `Hello, ${user.username}!`);
      return res.redirect("/campgrounds");
    });

  });

});

router.get("/signin", (req, res) => {
  return res.render("signin");
});

router.post("/signin", passport.authenticate("local", {
  failureRedirect: "/signin"
}), (req, res) => {
  req.flash("success", "Hello!");
  return res.redirect(req.session.next || "/campgrounds");
});

router.get("/signout", (req, res) => {
  req.logout();
  req.flash("success", "Bye!");
  return res.redirect("/");
});