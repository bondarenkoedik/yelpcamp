"use strict"

const mongoose   = require("mongoose"),
      Comment    = require("./models/comment"),
      Campground = require("./models/campground");

var description =  '<p>Bacon ipsum dolor amet cupim short ribs ball tip ham cow filet mignon t-bone hamburger.' + 
                   'Rump short ribs meatloaf, short loin sirloin tenderloin pastrami boudin ribeye flank biltong turkey filet mignon tri-tip fatback.' +
                   'Meatloaf tri-tip beef ribs ribeye short ribs. Short ribs meatloaf ground round hamburger, turducken tongue drumstick fatback sausage.' + 
                   'Ball tip cupim drumstick cow pancetta shankle ham brisket short ribs turducken, meatloaf prosciutto boudin.</p>' +
                   '<p>Shankle filet mignon t-bone pig meatball alcatra leberkas tenderloin salami picanha ground round drumstick biltong pancetta.' +
                   'T-bone flank beef biltong. Tongue pork chop alcatra beef pork belly picanha short loin, fatback tenderloin pork spare ribs.' +
                   'Strip steak cow porchetta, shoulder filet mignon pork loin landjaeger chuck short loin picanha frankfurter.' +
                   'Hamburger salami flank cupim. Andouille alcatra salami, ham hock chicken hamburger venison ribeye sirloin meatloaf pork turducken pancetta.' +
                   'Landjaeger beef ribs fatback meatloaf.</p>' +
                   '<p>Cupim short loin frankfurter ball tip hamburger bresaola boudin pancetta pork chop brisket.' +
                   'Prosciutto shank flank ham strip steak tenderloin salami pig meatloaf t-bone kielbasa capicola.' +
                   'Spare ribs pastrami ground round salami pancetta kielbasa cow ball tip tri-tip leberkas shank hamburger.' +
                   'Porchetta short loin boudin bacon kielbasa pastrami pork loin spare ribs turducken.' +
                   'Flank tail andouille strip steak beef ribs porchetta cupim boudin kevin shank biltong shoulder.</p>';

const data = [
  {
    name: "My campground",
    img: "https://farm6.staticflickr.com/5059/5518252117_d232831997.jpg",
    description: description
  },
  {
    name: "Awesome campgroun",
    img: "https://farm1.staticflickr.com/606/21042031428_b5fbe73afe.jpg",
    description: description
  },
  {
    name: "Super place",
    img: "https://farm8.staticflickr.com/7457/9586944536_9c61259490.jpg",
    description: description
  },
];

module.exports = () => {

    Comment.remove({}, err => {
      if (err) {
        console.log(err);
      } else {
        console.log("comments have been removed");
      }
    });

    // remove all campgronds  
    Campground.remove({}, (err) => {
      if (err) {
        console.log(err);
      } else {

        console.log("campgrounds has been removed");

        // add a few campgrounds
        // data.forEach(camp => {
        //   Campground.create(camp, (err, camp) => {
        //     if (err) {
        //       console.log(err);
        //     } else {
        //       console.log("Campground have been added", camp);

        //       // create a comments on each campgrounds
        //       Comment.create({
        //         text: "This place is great, but I want internet.",
        //         author: "Homer"
        //       }, (err, comment) => {

        //         if (err) {
        //           console.log(err);
        //         } else {
                  
        //           camp.comments.push(comment);
        //           camp.save((err, savedCamp) => {
        //             if (err) {
        //               console.log(err);
        //             } else {
        //               console.log("created new comment");
        //             }
        //           });
                
        //         }

        //       });
        //     }

        //   });
        // });

      }
    });

};